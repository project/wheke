#/bin/bash

# Where you cloned the wheke repository
DISTRIB_PATH=/home/drupal/wheke

# $HOME_PATH/$DIR_INSTALL will be the drupal installation directory
HOME_PATH=/home/drupal
DIR_INSTALL=www

# User and pass information
ACCOUNT_NAME=wheke_test
ACCOUNT_PASS=$(apg -a 0 -M sncl -n 1 -x 10 -m 8)
MYSQL_USER=wheke_test
MYSQL_PASS=$(apg -a 0 -M sncl -n 1 -x 10 -m 8)
MYSQL_DB=wheke_test_db
MYSQL_SERVER=localhost

echo "### Installing Wheke"

echo ">>> update wheke repository"
cd $DISTRIB_PATH
git remote update
git rebase origin/7.x-1.x

echo ">>> remove old site"
cd $HOME_PATH
mv $DIR_INSTALL /tmp/`date +%F-%H%M%S`_$DIR_INSTALL

echo ">>> make wheke.make"
drush make wheke/build-wheke.make $DIR_INSTALL --working-copy

echo ">>> install site"
cd $HOME_PATH/$DIR_INSTALL
drush si -y wheke --account-name=$ACCOUNT_NAME --account-pass=$ACCOUNT_PASS --db-url='mysql://'$MYSQL_USER':'$MYSQL_PASS'@'$MYSQL_SERVER'/'$MYSQL_DB --locale=fr

echo "### Install information"
echo "Drupal pass: $ACCOUNT_PASS"
echo "MYSQL  pass: $MYSQL_PASS"

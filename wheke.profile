<?php
/**
 * @file
 * Enables modules and site configuration for a wheke site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function wheke_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Returns an array of the modules to be enables when this profile is installed.
 *
 * @return
 *  Array of modules to enable
 */
/*
function wheke_profile_modules() {
  return array (
    // core module first
    'block',
    // contributed modules
    'admin_menu',
    // Image and content management modules
    // Related directly to ils
    'opac'
  );
}
*/

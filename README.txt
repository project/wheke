
Wheke: a distribution for libraries

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

First, we provided our first module called opac. The opac module enables 
libraries to integrate their catalog into Drupal allowing importing records, 
make advanced searches with faceted results, circulation task etc ... This 
module is intended to work with any ILS by using connectors. Everyone can 
make its own by creating a php file. We are working too with pazpar2 for 
federated search with mkdru and plan to develop an ERM (electronic ressources 
management) soon.

We planned to provide a Drupal distribution with modules preconfigured for 
helping libraries to build their website. It is the aim of this project.

REQUIREMENTS
------------

This distribution download everything it needs, nothing more special to have.

INSTALLATION
------------

. Build it

drupal@server-test:~$ git clone --recursive --branch 7.x-2.x \
  git://git.drupal.org:sandbox/clrh/1866840.git wheke
drupal@server-test:~$ drush make wheke/build-wheke.make www --working-copy
drupal@server-test:~$ cd /path/to/www/
drupal@server-test:~/www$ time drush si -y wheke --account-name=admin \
  --account-pass=pass --db-url='mysql://dbuser:dbpass@localhost/drupal' \
  --locale=fr

. Pdf libraries

You will need pdf libraries, which can't be in makefiles because not in
whitelist https://drupal.org/packaging-whitelist

libraries[fpdi][download][type] = "get"
libraries[fpdi][download][url] = "http://www.setasign.com/supra/kon2_dl/\
60111/FPDI-1.4.4.zip"
libraries[fpdi][directory_name] = "fpdi"
libraries[fpdi][destination] = "libraries"

libraries[fpdf][download][type] = "get"
libraries[fpdf][download][url] = "http://www.setasign.com/supra/kon2_dl/6\
3411/FPDF_TPL-1.2.3.zip"
libraries[fpdf][download][subtree] = "fpdf_tpl.php"
libraries[fpdf][directory_name] = "fpdf_tpl.php"
libraries[fpdf][destination] = "libraries/fpdi"

libraries[tcpdf][download][type] = "get"
libraries[tcpdf][download][url] = "https://depot.biblibre.com/chernandez/\
lib/tcpdf_6_0_005.zip"
libraries[tcpdf][directory_name] = "tcpdf"
libraries[tcpdf][destination] = "libraries"

CONFIGURATION
-------------

TROUBLESHOOTING
---------------

Nothing to report. If you have a problem, send us a drupal issue in the
project.

MAINTAINERS
-----------

 * Alex Arnaud https://drupal.org/user/1398446
 * Claire Hernandez https://drupal.org/u/claire-hernandez
 * Julian Maurice https://drupal.org/u/julian-maurice
 * Laurence Rault https://drupal.org/user/2307894
 * Matthias Meusburger https://drupal.org/user/2275170

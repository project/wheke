; Use this file to build a full distribution including Drupal core and the
; Wheke install profile using the following command:
;
; drush make distro.make <target directory>
api = 2
core = 7.x

projects[ctools][version] = "1.6"
projects[facetapi][version] = "1.5"
projects[entityreference][version] = "1.1"
projects[features][version] = "1.0"
projects[feeds][version] = "2.0-alpha8"
projects[feeds][patch][] = "http://drupal.org/files/feeds-allow-taxonomy-mapper-to-search-in-term-fields-2029417.patch"
projects[job_scheduler][version] = "2.0-alpha3"
projects[libraries][version] = "2.2"
projects[link][version] = "1.3"
projects[views][version] = "3.5"
projects[wysiwyg][version] = "2.2"
projects[entity][type] = "module"
projects[entity][download][type] = "git"
projects[entity][download][url] = "http://git.drupal.org/project/entity.git"
projects[entity][download][branch] = "7.x-1.x"
projects[entity][download][revision] = "5731f741e3366889e95b5357f1f85b0acc51a9fe"
projects[strongarm][version] = "2.0"
projects[ds][version] = "2.7"
projects[amazon][version] = "1.1"
projects[field_permissions][version] = "1.0-beta2"
projects[transliteration][version] = "3.2"
projects[token][version] = "1.5"
projects[viewfield][version] = "2.0"
projects[insert][version] = "1.3"
projects[scald][version] = 1.3
projects[date][version] = 2.8
projects[calendar][version] = 3.5
projects[context][version] = 3.6
projects[pathauto][version] = 1.2

; Translation i18n
projects[l10n_update][version] = "1.1"
projects[i18n][version] = "1.12"
projects[variable][version] = "2.5"

; Search
projects[search_api][type] = "module"
projects[search_api][download][type] = "git"
projects[search_api][download][url] = "http://git.drupal.org/project/search_api.git"
projects[search_api][download][branch] = "7.x-1.4"
projects[search_api][patch][] = "http://drupal.org/files/search_api-vbo-1123454-36.patch"

projects[search_api_solr][type] = "module"
projects[search_api_solr][download][type] = "git"
projects[search_api_solr][download][url] = "http://git.drupal.org/project/search_api_solr.git"
projects[search_api_solr][download][branch] = "7.x-1.2"
projects[search_api_solr][patch][] = "http://drupal.org/files/0001-Issue-2091995-Use-tum_-and-tus_-fields-for-autocompl.patch"

projects[searchapimultiaggregate][type] = "module"
projects[searchapimultiaggregate][download][type] = "git"
projects[searchapimultiaggregate][download][url] = "http://git.drupal.org/project/searchapimultiaggregate.git"
projects[searchapimultiaggregate][download][branch] = "7.x-1.x"

projects[better_exposed_filters][version] = "3.0"
;projects[better_exposed_filters][patch][] = "http://drupal.org/files/bef_search_api_patch.patch"
;projects[better_exposed_filters][patch][] = "http://drupal.org/files/bef-required_input-1447730-19.patch"

projects[vbo_search_api][type] = "module"
projects[vbo_search_api][download][type] = "git"
projects[vbo_search_api][download][url] = "http://git.drupal.org/sandbox/AlexArnaud/1910528.git"
projects[vbo_search_api][download][branch] = "7.x-1.x"

projects[search_api_extended_filters][type] = "module"
projects[search_api_extended_filters][download][type] = "git"
projects[search_api_extended_filters][download][url] = "git://git.drupal.org/sandbox/AlexArnaud/1871208.git"
projects[search_api_extended_filters][download][branch] = "7.x-1.x"

projects[search_api_autocomplete][type] = "module"
projects[search_api_autocomplete][download][type] = "git"
projects[search_api_autocomplete][download][url] = "http://git.drupal.org/project/search_api_autocomplete.git"
projects[search_api_autocomplete][download][branch] = "7.x-1.0"

; RM 627 User account and field group
projects[field_group][version] = "1.4"

; RM 52 basket feature
projects[views][version] = "3.8"
projects[views_bulk_operations][version] = "3.2"
projects[session_api][version] = "1.0-rc1"

projects[views_pdf][type] = "module"
projects[views_pdf][download][type] = "git"
projects[views_pdf][download][url] = "http://git.drupal.org/project/views_pdf.git"
projects[views_pdf][download][branch] = "7.x-1.2"
projects[views_pdf][patch][] = "http://drupal.org/files/issues/views_pdf-php-notices-2042927-29.patch"

projects[flag][version] = "3.5"
projects[views_flag_refresh][version] = "1.3"
projects[views_data_export][version] = "3.0-beta8"
projects[print][version] = "2.0"
projects[jquerymenu][version] = "4.0-alpha3"

; TODO Change branch and version after releasing this module
projects[koha_connector][type] = "module"
projects[koha_connector][download][type] = "git"
projects[koha_connector][download][url] = "git://git.drupal.org/project/koha_connector.git"
projects[koha_connector][download][branch] = "7.x-1.x"

projects[flag_cart][type] = "module"
projects[flag_cart][download][type] = "git"
projects[flag_cart][download][url] = "git://git.drupal.org/sandbox/AlexArnaud/1838252.git"
projects[flag_cart][download][branch] = "7.x-1.1"

; TODO Change branch and version after releasing this module
projects[opac][type] = "module"
projects[opac][download][type] = "git"
projects[opac][download][url] = "http://git.drupal.org/project/opac.git"
projects[opac][download][branch] = "7.x-3.0-alpha5"

projects[simplesearch][type] = module
projects[simplesearch][download][type] = git
projects[simplesearch][download][url] = http://git.drupal.org/sandbox/AlexArnaud/1923278.git
projects[simplesearch][download][branch] = 7.x-1.x
;projects[simplesearch][directory_name] = simplesearch

projects[simplesearch_taxonomy_options][type] = module
projects[simplesearch_taxonomy_options][download][type] = git
projects[simplesearch_taxonomy_options][download][url] = http://git.drupal.org/sandbox/AlexArnaud/2024357.git
projects[simplesearch_taxonomy_options][download][branch] = 7.x-1.x
;projects[simplesearch_taxonomy_options][directory_name] = simplesearch_taxonomy_options

projects[pz][type] = module
projects[pz][download][type] = git
projects[pz][download][url] = http://git.drupal.org/sandbox/AlexArnaud/1896388.git
projects[pz][download][branch] = 7.x-1.x
;projects[pz][directory_name] = pz

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.5/ckeditor_3.6.5.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

libraries[SolrPhpClient][download][type] = "get"
libraries[SolrPhpClient][download][url] = "http://solr-php-client.googlecode.com/files/SolrPhpClient.r60.2011-05-04.zip"
libraries[SolrPhpClient][directory_name] = "SolrPhpClient"
libraries[SolrPhpClient][destination] = "libraries"

;;libraries[fpdi][download][type] = "get"
;;libraries[fpdi][download][url] = "http://www.setasign.com/supra/kon2_dl/60111/FPDI-1.4.4.zip"
;;libraries[fpdi][directory_name] = "fpdi"
;;libraries[fpdi][destination] = "libraries"
;;
;;libraries[fpdf][download][type] = "get"
;;libraries[fpdf][download][url] = "http://www.setasign.com/supra/kon2_dl/63411/FPDF_TPL-1.2.3.zip"
;;libraries[fpdf][download][subtree] = "fpdf_tpl.php"
;;libraries[fpdf][directory_name] = "fpdf_tpl.php"
;;libraries[fpdf][destination] = "libraries/fpdi"
;;
;;libraries[tcpdf][download][type] = "get"
;;;libraries[tcpdf][download][url] = "http://downloads.sourceforge.net/project/tcpdf/tcpdf_6_0_005.zip"
;;libraries[tcpdf][download][url] = "https://depot.biblibre.com/chernandez/lib/tcpdf_6_0_005.zip"
;;libraries[tcpdf][directory_name] = "tcpdf"
;;libraries[tcpdf][destination] = "libraries"

; Add drush commands for Feeds
;;libraries[feeds.drush.inc][download][type] = get
;;libraries[feeds.drush.inc][download][url] = http://drupalcode.org/sandbox/jrbeeman/1712672.git/blob_plain/314025b1c92383e7998bdd994ef4724816e2ed4a:/feeds.drush.inc
;;libraries[feeds.drush.inc][directory_name] = feeds
;;libraries[feeds.drush.inc][destination] = modules

; Wheke_responsive_theme
projects[wheke_responsive_theme][type] = theme
projects[wheke_responsive_theme][download][type] = git
projects[wheke_responsive_theme][download][url] = http://git.drupal.org/sandbox/clrh/2117437.git
projects[wheke_responsive_theme][download][branch] = 7.x-1.x

; Wheke_conf - features
projects[wheke_conf][type] = "module"
projects[wheke_conf][download][type] = "git"
projects[wheke_conf][download][url] = "git://git.drupal.org/sandbox/clrh/2130967.git"
projects[wheke_conf][download][branch] = "7.x-1.x"

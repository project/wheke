api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"

; Add Wheke Kickstart to the full distribution build.
projects[wheke][type] = profile
projects[wheke][download][type] = git
projects[wheke][download][url] = git://git.drupal.org/sandbox/clrh/1866840.git
projects[wheke][download][branch] = 7.x-2.x
